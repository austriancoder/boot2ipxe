CROSS_COMPILE ?= ''

ifneq (x86_64, $(shell uname -m))
    $(error Expect building from a x86_64 host)
endif

ifeq (, $(shell which $(CROSS_COMPILE)gcc))
    $(error The $(CROSS_COMPILE) toolchain is missing)
endif
