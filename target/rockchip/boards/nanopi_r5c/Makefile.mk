UBOOT_COMMIT = afd47bc4e22f7f763e3ba94131f0151ae9eab65e
UBOOT_REPO_URL = https://github.com/Kwiboo/u-boot-rockchip.git
UBOOT_TARGET=nanopi-r5c-rk3568_defconfig

include $(ROOTDIR)/dependencies/rkbin/Makefile.mk

ROCKCHIP_TPL = $(RKBIN_SRC)/bin/rk35/rk3568_ddr_1560MHz_v1.18.bin
$(ROCKCHIP_TPL): $(RKBIN_SRC)
	@true

BL31 = $(RKBIN_SRC)/bin/rk35/rk3568_bl31_v1.43.elf
$(BL31): $(RKBIN_SRC)
	@true
