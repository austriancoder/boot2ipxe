#!/bin/bash

# origin: https://gitlab.freedesktop.org/mupuf/valve-infra/-/blob/a5eb44c33520d274e82286db6c44b1fb831c00bc/ipxe-boot-server/qemu-run.sh

set -u

disk_img=$1
boot_mode=$2

ovmf_dirs=("/usr/share/edk2-ovmf/x64" "/usr/share/OVMF")
ovmf=
for d in ${ovmf_dirs[@]}; do
    if [ -e $d/OVMF.fd ]; then
        ovmf=$d/OVMF.fd
        break
    fi
done

case $boot_mode in
    efi)
        if [ -z $ovmf ]; then
            echo 'OVMF not found. Probably missing the edk2 ovmf packages'
            exit 1
        fi
        qemu_extra_args="-bios $ovmf"
        ;;
    pcbios)
        qemu_extra_args=
        ;;
    *)
        echo 'Unknown boot mode'
        exit 1
        ;;
esac

run_qemu="qemu-system-x86_64 \
    -enable-kvm \
    -nographic \
    -no-reboot \
    -m size=256 \
    -drive format=raw,file="$disk_img" \
    -nic user,model=e1000 \
    $qemu_extra_args"

ipxe_banner='-- Open Source Network Boot Firmware --'

if which expect &> /dev/null; then
    echo running qemu in $boot_mode mode...

    expect -c "set timeout 10; \
        spawn $run_qemu; \
        expect -- \"$ipxe_banner\" {exit 0}; \
        exit 1" &> /dev/null
        ret=$?

    result=passed
    if [ $ret != 0 ]; then
        result=failed
    fi

    echo test $result
    exit $ret
else
    echo "ERROR: The expect binary is missing"
    exit 1
fi
