SHELL := /bin/bash

# raise an error if any variable in the list is unset
# params:
#	a list of variables to be checked
define fail-if-any-var-unset
$(foreach VAR,$(1),$(if $(value $(VAR)),,$(error $(VAR) not set)))
endef

# create a target that can be referenced by other targets to
# retrigger the rules if the variable passed as a parameter
# changed.
#
# This is achieved by creating files in $(TARGET_OUTPUT)/, named
# after the variable, and containing its content. If the file
# already exists, and the content is the same, the file isn't
# touched which means targets depending on it won't be rebuilt.
# If the content changed, the file will be updated with the new
# value, and thus trigger the re-generation of all targets depending
# on it.
#
# params:
#	name of the variable to keep track of
#
# example:
#
# $(eval $(call DEPENDABLE_VAR,CROSS_COMPILE))
#
# myfile: $(ARG~CROSS_COMPILE)
# 	echo "rebuild using $(CROSS_COMPILE)"
# 	touch $@
#
define DEPENDABLE_VAR
ARG~$1 := $(TARGET_TMP)/.ARG~$1
.PHONY: phony
$(TARGET_TMP)/.ARG~$1: phony $(TARGET_TMP)
	@if [ ! -f $(TARGET_TMP)/.ARG~$1 ]; then touch $(TARGET_TMP)/.ARG~$1; fi
	@if [[ "`cat $(TARGET_TMP)/.ARG~$1 2> /dev/null`" != '$($1)' ]]; then echo "NOTE: The variable $1 changed value: '`cat $(TARGET_TMP)/.ARG~$1 2>/dev/null`' -> '$($1)'"; echo -n "$($1)" > "$(TARGET_TMP)/.ARG~$1"; fi
endef

# Generate the boilerplate code to download and setup git repositories
#
# params:
#	name of the dependency ($1)
#
# requires:
#	$1_PATH: Folder containing the makefile for the dependency
#	$1_REPO_URL
#	$1_COMMIT
#	$1_PATCHES
define GIT_REPO_DEPENDENCY
$1_DEP_SRC := $($1_PATH)/src
$1_SRC := $(TARGET_TMP)/$(shell echo "$1" | tr A-Z a-z)
$1_GIT_FETCH_OPTS ?= --depth=1

$(eval $(call DEPENDABLE_VAR,$1_REPO_URL))
$(eval $(call DEPENDABLE_VAR,$1_COMMIT))
$(eval $(call DEPENDABLE_VAR,$1_PATCHES))

# NOTE: Since we want to detect if the list of patches changed between two runs,
# we need to keep track of it between them. Do that by gathering the list of patches
# and storing it as if it was a parameter
# TODO: FIXUP as it re-triggers execution otherwise
ifdef $1_PATCHES
	$1_PATCHES_FILES := $(wildcard $($1_PATCHES)/*)
else
	$1_PATCHES_FILES :=
endif
$(eval $(call DEPENDABLE_VAR,$1_PATCHES_FILES))

$$($1_SRC): $(ROOTDIR)/Makefile.util.mk $$($1_DEP_SRC) $$(ARG~$1_REPO_URL) $(ARG~$1_COMMIT) $$(ARG~$1_PATCHES_FILES) $$($1_PATCHES_FILES)
	@pushd $$($1_DEP_SRC); \
	git cat-file -e $(strip $$($1_COMMIT))^{commit} || git fetch $$($1_REPO_URL) $$($1_COMMIT) $$($1_GIT_FETCH_OPTS); \
	git worktree add -f $$($1_SRC) $$($1_COMMIT); \
	pushd $$($1_SRC); \
	git reset --hard $$($1_COMMIT) > /dev/null; \
	popd; \
	popd

	@if [ -n "$$($1_PATCHES_FILES)" ]; then \
		echo "Applying the wanted $1 patches"; \
		cd $$($1_SRC); \
		git am --abort || true; \
		git am $$($1_PATCHES_FILES); \
		cd -; \
	else \
		echo "No patches specified for the $1 dependency"; \
	fi; \
	touch $$($1_SRC); \

$$($1_DEP_SRC):
	git init --bare $$@

endef
