include $(ROOTDIR)/Makefile.util.mk

# path to dependencies/arm-trusted-firmware
ATF_PATH := $(ROOTDIR)/dependencies/arm-trusted-firmware/

# Parameters
#
## Mandatory
##
## -  ATF_PLAT: chipset target (rk3399, for example)

ATF_BL31_PATH = $(ATF_SRC)/build/$(ATF_PLAT)/release/bl31/bl31.elf

ATF_REPO_URL ?= https://github.com/ARM-software/arm-trusted-firmware.git
ATF_COMMIT ?= 2503c8f3204c60013de8caa2e165b2875ad735e5

$(eval $(call GIT_REPO_DEPENDENCY,ATF))

$(eval $(call DEPENDABLE_VAR,ATF_PLAT))
$(eval $(call DEPENDABLE_VAR,CROSS_COMPILE))

$(ATF_BL31_PATH): $(ROOTDIR)/Makefile.util.mk $(ATF_PATH)/Makefile.mk $(ATF_SRC) $(ARG~ATF_PLAT) $(ARG~CROSS_COMPILE)
	$(call fail-if-any-var-unset, CROSS_COMPILE, ATF_PLAT)
	$(MAKE) -C$(ATF_SRC) CROSS_COMPILE=$(CROSS_COMPILE) PLAT=$(ATF_PLAT)

	# Ensure the file is marked as updated even if nothing had changed
	@touch $(ATF_BL31_PATH)
