include $(ROOTDIR)/Makefile.util.mk

RKBIN_PATH := $(ROOTDIR)/dependencies/rkbin
RKBIN_REPO_URL ?= https://github.com/rockchip-linux/rkbin
RKBIN_COMMIT ?= b4558da0860ca48bf1a571dd33ccba580b9abe23

$(eval $(call GIT_REPO_DEPENDENCY,RKBIN))
