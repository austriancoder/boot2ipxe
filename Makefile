include Makefile.util.mk

export ROOTDIR = $(CURDIR)
export TARGET = $(MAKECMDGOALS)
export OUT = $(ROOTDIR)/output
export TARGET_OUT = $(OUT)/$(TARGET)
export TARGET_TMP = $(TARGET_OUT)/tmp

export CONSOLE_BAUDRATE ?= 115200

$(OUT) $(TARGET_OUT) $(TARGET_TMP):
	mkdir -p $@

.PHONY: _basedeps
_basedeps: $(OUT) $(TARGET_OUT) $(TARGET_TMP)

.PHONY: clean
clean:
	rm -rf $(OUT) || true

.PHONY: veryclean
veryclean: clean
	rm -rf dependencies/*/src || true

.PHONY: test
test:
	$(call fail-if-any-var-unset,TARGET)
	$(MAKE) -C target/$(TARGET) test

rpi_arm64: _basedeps
	$(MAKE) -C target/$@ build

generic_x86_64: _basedeps
	$(MAKE) -C target/$@ build

nanopi_r4s: _basedeps
	$(MAKE) -C target/rockchip RK_BOARD=nanopi_r4s build

nanopi_r5c: _basedeps
	$(MAKE) -C target/rockchip RK_BOARD=nanopi_r5c build

efi_x86_64: _basedeps
	$(MAKE) -C target/generic_efi build ARCH=x86_64

efi_arm64: _basedeps
	$(MAKE) -C target/generic_efi build ARCH=arm64
